CREATE PROCEDURE OreSale (
    @Ore VARCHAR(250),
    @Inventory INT,
    @SaleAmount DEC,
    @Sale INT AUTO_INCREMENT
)
AS 
    BEGIN

    DECLARE @index INT

        SET @index1 = @Sale
        IF @index1 >= 14;
        UPDATE Supplies SET (@Inventory, @Ore, Date_Purchased, Tavern_Id, Supply_Cost - @SaleAmount);
    
    THEN 
    
        UPDATE Sales SET(@Sale, Service_Id, Guest_Id, Price, Sale_Date, Sale_Amount + @SaleAmount, Tavern_Id)
        RETURN; 
    END

EXEC dbo.OreSale(15, 'Coal Ore', '2019-03-18', 3, 1000);  



ALTER TABLE Supplies ALTER COLUMN Supply_Cost DECIMAL(10,2); 

CREATE PROCEDURE OreSale (
    @Ore VARCHAR(250),
    @Inventory INT,
    @SaleAmount DEC,
    @Tavern_Id INT,
    @Sale DEC 
)
AS 
    BEGIN
        DECLARE @SupplyCost INT,
        SELECT Supplies.Supply_Cost AS @SupplyCost FROM Supplies WHERE Supply_Id = 3;
        INSERT INTO Supplies VALUES (@Inventory, @Ore, GETDATE(), @Tavern_Id, @SupplyCost - @SaleAmount);

    END

    EXEC dbo.OreSale(15, 'Coal Ore', '2019-03-18', 3, 1000);  
