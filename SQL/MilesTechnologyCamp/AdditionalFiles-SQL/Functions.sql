/*SELECT 1, Service_Name, dbo.Roon_Skaep_Taverns(2), AS 
CurrentSupply
FROM  ;   

CREATE TABLE InventoryStock(
	ProductModelId INT PRIMARY KEY,
	Name VARCHAR(250),
	Quantity INT,
	Price INT
);

INSERT INTO InventoryStock(ProductModelId, Name, Quantity, Price)

			VALUES(1, 'Lawn Mower', 20, 130.00),
				  (2, 'Tool Box', 30, 5.00),
				  (3, 'Pesticides', 100, 12.00),
				  (4, 'Concrete Mix', 1000, 15.00);
				 
CREATE FUNCTION [schema_name.]sales (x, y)
RETURN data_type AS INT
BEGIN
    statements
    RETURN value
END				 */
/*
CREATE FUNCTION UnitSales( @quantity INT, @Price DEC(10,2))

	RETURNS DEC(10,2)
		AS 
	BEGIN
		RETURN @quantity * @Price;
	END;

SELECT dbo.UnitSales(10, 2);
SELECT Name, dbo.UnitSales(InventoryStock.Quantity, InventoryStock.Price) FROM InventoryStock WHERE ProductModelId >= 2;
SELECT Name, dbo.UnitSales(InventoryStock.Quantity, InventoryStock.Price) AS Purchases FROM InventoryStock WHERE ProductModelId >= 2;
SELECT Name, ProductModelId, Quantity, Price, dbo.UnitSales(InventoryStock.Quantity, InventoryStock.Price) AS Sales FROM InventoryStock WHERE ProductModelId >= 2;

CREATE FUNCTION Rooms( @GuestRooms INT, @Guest_Id INT, @GuestName VARCHAR(250), @RoomPrice DEC(10,2), @RoomName VARCHAR(250), @RoomColor VARCHAR(250) )
	RETURN DEC(10,2)
		AS
	BEGIN
		 DECLARE @GuestRooms * @RoomPrice;
		 SELECT @RoomPrice = MAX(@Guest_Id) 
		 FROM Guests.Guest_Names
		 WHERE Guest_Names = @Guest_Id;
		 IF (@GuestRooms IS NULL)
			SET @GuestRooms = 0;
		RETURN @GuestRooms;
	END; 

	RETURN '
	*/

	IF OBJECT_ID(N'Sales.getSalesByStore, N'IF) IS NOT NULL
		DROP FUNCTION Sales.getSalesByStore;
	GO 
	CREATE FUNCTION Sales.getSalesByStore (@storeid int)
	RETURNS TABLE
	AS
	RETURN
	(
		SELECT P.ProductID, P.Name, SUM(SD.LineTotal) AS 'Total'
		FROM Production.Product AS P
		JOIN.Sales.SalesOrderDetail AS SD ON SD.ProductID = P.ProductID
		JOIN Sales.SalesOrderHeadeer AS SH OH SH.SalesOrderID = SD.SalesOrderID
		JOIN Sales.Customer AS C ON  
	); 


SELECT dbo.Sales(1000, 23.39);  

SELECT CONCAT('CREATE TABLE ',TABLE_NAME, ' (') as TavernSales FROM INFORMATION_SCHEMA.TABLES 
	WHERE TABLE_NAME = 'Sales' 
UNION ALL 
SELECT CONCAT(cols.COLUMN_NAME, ' ', cols.DATA_TYPE, ' ', /*keys.COLUMN_NAME, , ' '*/ blorp.Constraint_Type,
	( CASE 
     	WHEN CHARACTER_MAXIMUM_LENGTH IS NOT NULL 
     		Then CONCAT ('(', CAST(CHARACTER_MAXIMUM_LENGTH as varchar(100)), ')') 
		Else '' 
	END) , 
      	CASE 
      		WHEN refConst.CONSTRAINT_NAME IS NOT NULL 
        		Then (CONCAT(' FOREIGN KEY REFERENCES ', constKeys.TABLE_NAME, '(', constKeys.COLUMN_NAME, ')')) 
			Else '' 
		END , 
    ',') as queryPiece 
    	FROM INFORMATION_SCHEMA.COLUMNS as cols 
			LEFT JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE as keys
            	ON (keys.TABLE_NAME = cols.TABLE_NAME and keys.COLUMN_NAME = cols.COLUMN_NAME) 
			LEFT JOIN INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS as refConst 
            	ON (refConst.CONSTRAINT_NAME = keys.CONSTRAINT_NAME) 
			LEFT JOIN (SELECT DISTINCT CONSTRAINT_NAME, TABLE_NAME, COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE) as constKeys 
            	ON (constKeys.CONSTRAINT_NAME = refConst.UNIQUE_CONSTRAINT_NAME)
			LEFT JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS as blorp
				ON blorp.CONSTRAINT_NAME = keys.CONSTRAINT_NAME  
	WHERE cols.TABLE_NAME = 'Sales' 
UNION ALL SELECT ')';


DROP FUNCTION KEYTYPE; 

CREATE FUNCTION KEYTYPE( @String VARCHAR(300))
		RETURNS
			VARCHAR(100)
		AS
		BEGIN
			DECLARE @Result VARCHAR(300)

				IF	subString(@String, 1, 2) = 'PK'

				SET @RESULT = 'Primary KEY' 

				ELSE

				SET @RESULT = 'Foreign KEY'

		RETURN @Result
		END; 

SELECT dbo.keytype('PK');		


SELECT CONCAT('CREATE TABLE ',TABLE_NAME, ' (') as TavernSales FROM INFORMATION_SCHEMA.TABLES 
	WHERE TABLE_NAME = 'Sales' 
UNION ALL 
SELECT CONCAT(cols.COLUMN_NAME, ' ', cols.DATA_TYPE, ' ', /*keys.COLUMN_NAME, , ' '*/ dbo.keytype(keys.CONSTRAINT_NAME), 
	( CASE 
     	WHEN CHARACTER_MAXIMUM_LENGTH IS NOT NULL 
     		Then CONCAT ('(', CAST(CHARACTER_MAXIMUM_LENGTH as varchar(100)), ')') 
		Else '' 
	END) , 
      	CASE 
      		WHEN refConst.CONSTRAINT_NAME IS NOT NULL 
        		Then (CONCAT(' FOREIGN KEY REFERENCES ', constKeys.TABLE_NAME, '(', constKeys.COLUMN_NAME, ')')) 
			Else '' 
		END , 
    ',') as queryPiece 
    	FROM INFORMATION_SCHEMA.COLUMNS as cols 
			LEFT JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE as keys
            	ON (keys.TABLE_NAME = cols.TABLE_NAME and keys.COLUMN_NAME = cols.COLUMN_NAME) 
			LEFT JOIN INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS as refConst 
            	ON (refConst.CONSTRAINT_NAME = keys.CONSTRAINT_NAME) 
			LEFT JOIN (SELECT DISTINCT CONSTRAINT_NAME, TABLE_NAME, COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE) as constKeys 
            	ON (constKeys.CONSTRAINT_NAME = refConst.UNIQUE_CONSTRAINT_NAME)
			LEFT JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS as blorp
				ON blorp.CONSTRAINT_NAME = keys.CONSTRAINT_NAME  
	WHERE cols.TABLE_NAME = 'Sales' 
UNION ALL SELECT ')';


DROP Function CalculateSale; 
CREATE FUNCTION CalculateSale( @Price DEC(10,2), @Sale_Amount DEC(10,2) )
	RETURNS DEC(10,2)			
		AS
			BEGIN
				RETURN (@Sale_Amount / @Price)
			END; 

SELECT dbo.CalculateSale(5, 20);   

SELECT Sale_Id, Price, Sale_Amount, dbo.CalculateSale(Sales.Price, Sales.Sale_Amount) AS Product FROM Sales;
SELECT Name, dbo.UnitSales(InventoryStock.Quantity, InventoryStock.Price) AS Purchases FROM InventoryStock WHERE ProductModelId >= 2;




SELECT CONCAT('CREATE FUNCTION ',TABLE_NAME, ' (') as TavernSales FROM Sales 
UNION ALL 
SELECT dbo.CalculateSale(keys.Guest_Id) 
    	FROM Sales AS cols 
			LEFT JOIN Guests as keys
            	ON (keys.Guest_Id = cols.Sale_Id) 
			LEFT JOIN Payments_Recieved as refConst 
            	ON (refConst.Payment_Recieved_Id = keys.Guest_Id) 
			LEFT JOIN (Guest_Id, Sale_Id, Payments_Recieved_Id FROM Sales) as constKeys 
            	ON (constKeys.Guest_Id = refConst.Sale_Id)
			LEFT JOIN Sales as blorp
				ON blorp.Guest_Id = cols.Guest_Id  
	WHERE cols.TABLE_NAME = 'Sales' 
UNION ALL SELECT ')';

/*Additional code For dropping constraints as well as some other variable information.*/

DROP FUNCTION Sales;

DECLARE @database NVARCHAR(50)
DECLARE @table NVARCHAR(50)
DECLARE @sql NVARCHAR(255)

SET @database = 'WRovinski_2019'
SET @table = ''

WHILE EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS

WHERE constraint_catalog=@database AND table_name=@table)

BEGIN

SELECT @sql = ' ALTER TABLE ' + @table + ' DROP CONSTRAINT ' + CONSTRAINT_NAME

FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS

WHERE constraint_catalog=@database AND table_name=@table

EXEC sp_executesql @sql

END

DROP Function CalculateSale; 
CREATE FUNCTION CalculateSale( @Price DEC(10,2), @Sale_Amount DEC(10,2) )
	RETURNS DEC(10,2)			
		AS
			BEGIN
				RETURN (@Sale_Amount / @Price)
			END; 

SELECT dbo.CalculateSale(5, 20);   

SELECT Sale_Id, Price, Sale_Amount, dbo.CalculateSale(Sales.Price, Sales.Sale_Amount) AS Product FROM Sales;
SELECT Name, dbo.UnitSales(InventoryStock.Quantity, InventoryStock.Price) AS Purchases FROM InventoryStock WHERE ProductModelId >= 2;