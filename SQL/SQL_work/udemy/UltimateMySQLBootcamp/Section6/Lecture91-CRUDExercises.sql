CREATE DATABASE shirts_db; 

CREATE TABLE shirts (shirt_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
                     article VARCHAR(20) NOT NULL,
                     color VARCHAR(20) NOT NULL, 
                     shirt_size VARCHAR(5) NOT NULL,
                     last_worn INT
                     ); 

INSERT INTO shirts(article, color, shirt_size, last_worn) VALUES('t-shirt', 'white', 'S', 10),
                                                                ('t-shirt', 'green', 'S', 200),
                                                                ('polo shirt', 'black', 'M', 10),
                                                                ('tank top', 'blue', 'S', 50),
                                                                ('t-shirt', 'blue', 'S', 0),
                                                                ('polo shirt', 'pink', 'M', 5),
                                                                ('tank top', 'white', 'S', 200),
                                                                ('tank top', 'blue', 'M', 15); 

/*When using an auto increment attribute on a value, remember you need to specify the parameters, 
so the rows will match column count.*/

INSERT INTO shirts(article, color, shirt_size, last_worn) VALUES('polo shirt', 'purple', 'M', 50); 

SELECT article, color FROM shirts; 

SELECT article, color FROM shirts;
 
SELECT * FROM shirts WHERE shirt_size='M';
 
SELECT article, color, shirt_size, last_worn FROM shirts WHERE shirt_size='M';

SELECT article, color, shirt_size, last_worn FROM shirts;

UPDATE shirts SET shirt_size = 'L' WHERE article="polo shirt";  

UPDATE shirts SET last_worn = 0 WHERE last_worn = 15;

UPDATE shirts SET color = "off white" WHERE color = 'white';
UPDATE shirts SET shirt_size = "XS" WHERE color = 'off white';

/*Or to update it in one line use a comma and it will select as many columns as you need.*/
UPDATE shirts SET color = "off white", shirt_size = 'XS' WHERE color = 'white';


DELETE FROM shirts WHERE last_worn >= 200; 

DELETE FROM shirts WHERE article = 'tank top';

DELETE FROM shirts;

DROP DATABASE IF EXISTS shirts_db; 
