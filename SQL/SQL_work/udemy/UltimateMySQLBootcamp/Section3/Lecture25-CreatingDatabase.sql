CREATE DATABASE database_name;
CREATE DATABASE soap_store; 

DROP DATABASE [this name of database here];

SHOW DATABASES; 

CREATE DATABASE Dog_Walking_App; 

SELECT DATABASE Dog_Walking_App; 

CREATE DATABASE cat_app;

CREATE TABLE cats ( name VARCHAR(100),
                    age INT); 

SHOW COLUMNS FROM cats; 

DESCRIBE cats; 

DROP TABLE cats;
DROP TABLE IF EXISTS cats;

INSERT into cats VALUES('Jester', 3),
                       ('LionsRawr', 4),
                       ('GamerGirl', 4),
                       ('Blue', 1);

SELECT * FROM cats; 
SELECT * FROM cats WHERE age >=4; 

INSERT into cats VALUES('Peanut', 3),
                       ('Edgelord', 2),
                       ('Billy', 4),
                       ('Jelly', 7);

CREATE TABLE People_Kind (first_name VARCHAR(20),
                          last_name VARCHAR(20),
                          age INT);

INSERT INTO People_Kind VALUES('Linda', 'Bleicher', 45),
                              ('Philip', 'Frond', 38),
                              ('Calvin', 'Fischoeder', 70),
                              ('Lorden', 'Miller', 40);

SELECT * FROM People_Kind;
DESCRIBE People_Kind;