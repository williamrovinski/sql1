UPDATE cats SET breed='Shorthair' WHERE breed='Tabby';  /*The SET statement is used with the UPDATE statement
                                                        to alter a specific column.*/

UPDATE cats SET age=14 WHERE name='Misty';

UPDATE cats SET name='Jack' WHERE name = "JAckson"; /*Even though the original name was Jackson the VARCHAR 
                                                      isn't case sensitive, it only cares if the values are 
                                                      the same.*/
UPDATE cats SET breed='British Shorthair' WHERE name = "Ringo";

UPDATE cats SET age=12 WHERE breed = "Maine Coon";