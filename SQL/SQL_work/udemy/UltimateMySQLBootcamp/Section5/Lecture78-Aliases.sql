SELECT cat_id AS id, name FROM cats; 

SELECT cat_id AS 'SplorpLorp', name FROM cats; /*As works as an alias, in other words AS mean you substitute 
                                                the column name as something else.*/

SELECT name AS 'cat name', breed AS 'kitty breed' FROM cats;
 
DESCRIBE cats;