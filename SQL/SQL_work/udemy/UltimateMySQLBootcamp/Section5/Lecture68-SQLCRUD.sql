INSERT INTO cats(name, age) VALUES('Taco', 14);

DROP TABLE IF EXISTS cats; 

CREATE TABLE cats ( cat_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
                    name VARCHAR(100),
                    breed VARCHAR(100),
                    age INT
                );

DESCRIBE cats;

INSERT INTO cats(name, breed, age) VALUES('Ringo', 'Tabby', 4),
                                         ('Cindy', 'Maine Coon', 10),
                                         ('Dumbledore', 'Maine Coon', 11),
                                         ('Egg', 'Persian', 4),
                                         ('Misty', 'Tabby', 13),
                                         ('George Michael', 'Ragdoll', 9),
                                         ('Jackson', 'Sphynx', 7);

SELECT * FROM cats;         /*Read*/

SELECT name FROM cats;
SELECT age FROM cats;

SELECT name, age FROM cats;
SELECT cat_id, name, age, breed FROM cats; 