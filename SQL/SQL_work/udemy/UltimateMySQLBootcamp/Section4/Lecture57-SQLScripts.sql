INSERT INTO cats(name) VALUES ('Alabama'); /*You can set a parameter next to the table name and have the INSERT statement
                                            only add those values, the rest of the row values would be NULL*/
SELECT * FROM cats;
/*But lets get it so you cannot put NULL as a value in the table.
simply put you could write.*/

CREATE TABLE cats ( name VARCHAR(100) NOT NULL,
                    age INT NOT NULL );

INSERT INTO cats(name) VALUES ('Texas');


