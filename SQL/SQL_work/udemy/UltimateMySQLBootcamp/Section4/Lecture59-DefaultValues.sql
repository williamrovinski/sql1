CREATE TABLE cats3 ( name VARCHAR(20) DEFAULT 'no name provided',
                     age INT DEFAULT 99
                    ); 

DESCRIBE cats3;

INSERT INTO cats3(age) VALUES(13);
INSERT INTO cats3() VALUES();

SELECT * FROM cats3; 

CREATE TABLE cats4 ( name VARCHAR(100) NOT NULL DEFAULT 'unnamed',
                     age INT NOT NULL DEFAULT 99
                    ); 
                    /*The reason for this is if you manually tried to enter NULL */
INSERT INTO cats4() VALUES(); 

INSERT INTO cats4(name, age) VALUES('Montana', NULL); /*This won't work.*/

CREATE TABLE unique_cats (cat_id INT NOT NULL,
                          name VARCHAR(100),
                          age INT,
                          PRIMARY KEY (cat_id)
                         );

INSERT INTO unique_cats(cat_id, name, age) VALUES(1, "Fred", 2); 

INSERT INTO unique_cats(cat_id, name, age) VALUES(2, "Louise", 4); /*Primary Keys cannot have duplicates.*/

CREATE TABLE unique_cats2 (cat_id INT NOT NULL AUTO_INCREMENT,
                           name VARCHAR(100),
                           age INT,
                           PRIMARY KEY (cat_id)
                           );
DESCRIBE unique_cats2;

INSERT INTO unique_cats2(name, age) VALUES("Skippy", 4);
INSERT INTO unique_cats2(name, age) VALUES("Jiffy", 3);