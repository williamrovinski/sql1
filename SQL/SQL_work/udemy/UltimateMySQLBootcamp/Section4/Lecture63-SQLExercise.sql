CREATE TABLE employees( employee_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
                        first_name VARCHAR(20) NOT NULL,
                        last_name VARCHAR(20) NOT NULL,
                        middle_name VARCHAR(20),
                        age INT NOT NULL,
                        current_status VARCHAR(200) NOT NULL DEFAULT 'no status reported.'
                      );

INSERT INTO employees(first_name, last_name, age) VALUES
('Dora', 'Smith', 58);                                  /*When using an auto increment attribute on a value,
                                                        remember you need to specify the parameters, so the rows will
                                                        match column count.*/