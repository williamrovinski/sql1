DROP TABLE IF EXISTS artist; 
DROP TABLE IF EXISTS composition; 
DROP TABLE IF EXISTS albumn; 

create table artist(
		artist_id int (6) not null,
        artist_fname varchar(25) not null,
        artist_lname varchar(25),
        primary key (artist_id)
);

insert into artist (artist_id, artist_fname, artist_lname)
			value(401, 'Brandon', 'Flowers');
insert into artist (artist_id, artist_fname, artist_lname)
			value(402, 'Lena', 'Delray');
insert into artist (artist_id, artist_fname, artist_lname)
			value(403, 'Jarrod', 'Leto');
insert into artist (artist_id, artist_fname, artist_lname)
			value(404, 'Dave', 'Grohl');
insert into artist (artist_id, artist_fname, artist_lname)
			value(405, 'Dave', 'Matthews');
insert into artist (artist_id, artist_fname, artist_lname)
			value(406, 'Dan', 'Reynolds');
insert into artist (artist_id, artist_fname, artist_lname)
			value(407, 'Ben', 'McKee');
insert into artist (artist_id, artist_fname, artist_lname)
			value(408, 'Dan', 'Sermon');



create table Composition(
		composition_id int (6) not null,
        artist_id int (6),
        bandname_id int(6),
        song_name varchar(25) not null,
        recording_date date not null,
        track_length time not null,
        price decimal (5,2) not null,
        primary key (composition_id),
        constraint FK_artist foreign key (artist_id)
					references artist(artist_id)
		-- constraint FK_band_name foreign key (bandname_id)
					-- references BandName(bandname_id)
);

insert into Composition (composition_id, artist_id, bandname_id, song_name, recording_date, track_length, price)
			value(301, 401, 501, 'Song1', '2000-09-15', '0:03:25', 0.99);
insert into Composition (composition_id, artist_id, bandname_id, song_name, recording_date, track_length, price)
			value(302, 402, 501, 'Song2', '2010-07-21', '0:04:50', 1.50);
insert into Composition (composition_id, artist_id, bandname_id, song_name, recording_date, track_length, price)
			value(303, 403, null, 'Song3', '2012-11-01', '0:04:23', 3.99);
insert into Composition (composition_id, artist_id, bandname_id, song_name, recording_date, track_length, price)
			value(304, 404, 502, 'Song4', '2016-11-16', '0:07:50', 2.99);
insert into Composition (composition_id, artist_id, bandname_id, song_name, recording_date, track_length, price)
			value(305, 405, 502, 'Song5', '2016-01-14', '0:06:37', 2.99);
insert into Composition (composition_id, artist_id, bandname_id, song_name, recording_date, track_length, price)
			value(306, 406, null, 'Song6', '2016-07-04', '0:03:39', 1.99);
insert into Composition (composition_id, artist_id, bandname_id, song_name, recording_date, track_length, price)
			value(307, 407, null, 'Song7', '2013-04-09', '0:05:01', 0.99);
insert into Composition (composition_id, artist_id, bandname_id, song_name, recording_date, track_length, price)
			value(308, 408, null, 'Song8', '2015-03-01', '0:04:10', 2.50);
            



CREATE TABLE album (
			album_id int (6),
            composition_id int (6),
            album_title varchar (25) not null,
            years_released date not null,
            price decimal (5,2) not null,
            primary key (album_id),
            constraint FK_composition foreign key (composition_id)
					references composition(composition_id)
);

insert into album (album_id, composition_id, album_title, years_released, price)
			value(201, 301, 'Sawdust', '2001-1-15', 9.99);
insert into album (album_id, composition_id, album_title, years_released, price)
			value(202, 302, 'WonderfulWonderful', '2010-9-21', 13.99);
insert into album (album_id, composition_id, album_title, years_released, price)
			value(203, 303, 'DontWasteYourWishes', '2012-12-01', 18.50);
insert into album (album_id, composition_id, album_title, years_released, price)
			value(204, 304, 'DirectHits', '2016-11-16', 23.99);
insert into album (album_id, composition_id, album_title, years_released, price)
			value(205, 305, 'BattleBorn', '2016-02-14', 21.99);
insert into album (album_id, composition_id, album_title, years_released, price)
			value(206, 306, 'DayandAge', '2016-07-04', 13.99);
insert into album (album_id, composition_id, album_title, years_released, price)
			value(207, 307, 'HotFuss', '2013-05-09', 10.99);
insert into album (album_id, composition_id, album_title, years_released, price)
			value(208, 308, 'SamsTown', '2015-04-01', 17.99);
            