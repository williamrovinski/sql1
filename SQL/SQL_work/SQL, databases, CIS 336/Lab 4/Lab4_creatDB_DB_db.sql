
--CIS336
--William Rovinski
--Lab 4

-- create database
DROP DATABASE IF EXISTS on;
CREATE DATABASE om;

SELECT * FROM cis336.customers;
Select customer_first_name, customer_last_name, customer_phone FROM
customers

ORDER BY customer_last_name, customer_first_name, customer_phone;

SELECT * FROM cis336.customers;

SELECT CONCAT(customer_first_name, " ", customer_last_name) As

"Customer",customer_phone As "Phone", customer_city As "City"

FROM customers 

WHERE customer_city in ("New York", "New Jersey", "Washington")

ORDER BY phone;

SELECT * FROM cis336.customers;

SELECT DISTINCT)customer_city) As "Cities" from customers

ORDER BY cities Desc;

SELECT * FROM cis336.customers;

SELECT title, unit_price As "Original",

ROUND((unit_price - (25/100* unit_price)), 2) As "**SALE PRICE"

FROM items

ORDER BY Original;

SELECT * FROM cis336.customers;

SELECT customer_first_name As "FirstName", customer_last_name As

LastNAME, customer_city As "City"

FROM customers

WHERE customer_zip like '4%';

SELECT * FROM cis336.customers;

SELECT order_id, DATE_FORMAT(Order_Date, '%M %d, %Y') As "Ordered"

FROM orders

WHERE order_date >= '2014-03-01'

AND order_date <= '2014-04-30';

AND order_date <= '2014-04-30';

SELECT * FROM cis336.customs;

SELECT order_id, DATE_FORMAT(order_date, '%m/%d/%y') As "Ordered"

FROM orders

WHERE order_date

BETWEEN '2014-05001' AND ' '2014-05-31';

SELECT * FROM cis336.customers;

SELECT order_id, customer_id, order_date

FROM orders

WHERE shipped_date IS NULL

ORDER BY order_date DESC;

SELECT * FROM cis336.customers;

SELECT ordeer_id, order_date, shipped_date, shipped_date, dayname(order_date) As

"ORDER_DAY"

FROM orders

WHERE dayname(order_date) IN ("Saturday", "Sunday")

AND shipped_date IS NOT NULL

ORDER BY order_date Desc;

SELECT * FROM cis336.customers;

SELECT customer_last_name, customer_phone, customer_fax

FROM customers

WHERE customer_fax IS NOT NULL;

SELECT * FROM cis336.customers; 

INSERT INTO items (item_id, title, artist_id, unit_price)

VALUES (11, "Ode to My ERD', 15,12.95); 

SELECT * FROM cis336.customers;

UPDATE items

SET unit_price = 7.95

WHERE item_id = 11

SELECT * FROM cis336.customers;

DELETE FROM items

WHERE item_id = 11;

SELECT* FROM cis336.customers

SELECT CONCAT(customer_last_name ,',', customer_first_name) AS "Customer",

CONCAT('('SUBSTRING(customer_phone, 1, 3), ')',

SUBSTRING(customer_phone, 4,3), '-', SUBSTRING(customer_phone, 7,4)) 
AS 'Phone' FROM customers ORDER BY customer_last_name


