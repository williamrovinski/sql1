SELECT art.artist_id, com.composition_id, album.album_id, 
CONCAT (art.artist_fname, ' ',art.artist_lname) AS 'artist',
com.composition_id AS 'composition' 
FROM artist art 
	JOIN composition com USING (artist_id) 
	JOIN album USING (composition_id)
order by album.album_title desc; 
