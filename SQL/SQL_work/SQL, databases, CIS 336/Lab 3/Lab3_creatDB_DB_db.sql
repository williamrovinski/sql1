
--CIS336
--William Rovinski
--Lab 3

-- create database
DROP DATABASE IF EXISTS on;
CREATE DATABASE om;

-- select database
USE om;

CREATE TABLE ZIPCODE 
(
 Zip INT(11) PRIMARY KEY,
 City VARCHAR(25) NOT NULL,
 State CHAR(2) NOT NULL
);

CREATE TABLE Instructor 
(
Instructor_Id INT(6) Primary KEY,
Zip INT(11)
Salutation VARCHAR(5),
First_Name VARCHAR(25) NOT NULL,
Last_Name VARCHAR(25) NOT NULL,
Street_Address VARCHAR(50),
);

-- ALTER STATEMENTS
ALTER TABLE Instructor
ADD CONSTRAINT FK_Zip FOREIGN KEY (Zip)
REFERENCES Zipcode (Zip);

CREATE TABLE StudentID
(
StudentID_Id INT(6) PRIMARY KEY,
Salutation VARCHAR(5),
Last_Name VARCHAR(25) NOT NULL,
First_Name VARCHAR(25) NOT NULL,
Zip INT(11),
Street_Address VARCHAR(50),
Phone VARCHAR(15) NOT NULL,
Employer VARCHAR(50),
Registration_Date DATE NOT NULL
);

-- ALTER STATEMENTS
ALTER TABLE StudentID
ADD CONSTRAINT FK_Zip FOREIGN KEY (Zip)
REFERENCES Zipcode (Zip);

CREATE TABLE Course_ID
(
Course_ID INT(6) PRIMARY KEY,
Cost DECIMAL(8,2),
Description VARCHAR(50) NOT NULL,
Prerequisite INT(6)
);

--ALTER STATEMENTS
ALTER TABLE Course_ID
ADD CONSTRAINT FK_Prerequisite FOREIGN KEY (Prerequisite)
REFERENCE Course (Prerequisite);  

CREATE TABLE Section
(
Section_Id INT(8) PRIMARY KEY,
Start_Date_time DATETIME NOT NULL,
Course_Id INT(6)
Location VARCHAR(10),
Instructor_Id INT(6),
Course_Section_Num INT(6) NOT NULL,
Capacity INT(3),
);

-- ALTER STATEMENTS
ALTER CONSTRAINT FK_Instructor_ID FOREIGN KEY (Course_Id)
REFERENCES Section (Course_Id);

CREATE TABLE Enrollment
(
Section_ID INT(8),
Student_Id INT(6),
Enroll_Date DATE NOT NULL,
Final_Grade CHAR(1)
);

-- ALTER STATEMENTS
ALTER TABLE Enrollment
ADD CONSTRAINT PK_Enrollment PRIMARY KEY (Section_Id, Student_Id);

ALTER TABLE ENROLLMENT
ADD CONSTRAINT FK_Section_Id FOREIGN KEY (Section_Id)
REFERENCES Section (Section_Id);

ALTER TABLE ENROLLMENT
ADD CONSTRAINT FK_Section_Id FOREIGN KEY (Section_Id)
REFERENCES Section (Section_Id);

-- Instructor Table
INSERT INTO INSTRUCTOR
VALUES (101,'Mr','Fernand','Hanks','100 East 87th',10015);
INSERT INTO INSTRUCTOR
VALUES (102,'Mr','Tom','Wojick','518 West 120th',10025);
INSERT INTO INSTRUCTOR
VALUES (103,'Ms','Nina','Schorin,210 West 101st',10025);
INSERT INTO INSTRUCTOR
VALUES (104,'Mr','Gary','Pertez','34 Sixth Ave',10035);
INSERT INTO INSTRUCTOR
VALUES (105,'Ms','Anita','Morris','34 Maiden Lane',10015);
INSERT INTO INSTRUCTOR
VALUES (106,'Rev','Todd','Smythe','210 West 101st',10025);
INSERT INTO INSTRUCTOR
VALUES (107,'Dr','Marilyn','Frantzen','254 Bleeker',10005); 
INSERT INTO INSTRUCTOR
VALUES (108,'Dr','Manson','Hilltop','256 Bleeker',10005); 

-- Zipcode TABLE
INSERT INTO Zipcode
VALUES (7024,'Ft. Lee','NJ');
INSERT INTO Zipcode
VALUES (7047,'North Bergen','NJ');
INSERT INTO Zipcode
VALUES (10005,'New York','NY');
INSERT INTO Zipcode
VALUES (10015,'New York','NY');
INSERT INTO Zipcode
VALUES (10025,'New York','NY');
INSERT INTO Zipcode
VALUES (10035,'New York','NY');
INSERT INTO Zipcode
VALUES (11419,'Richmond Hill','NY');
INSERT INTO Zipcode
VALUES (11435,'Jamaica','NY');

-- Course TABLE
INSERT INTO Course 
VALUES (330,'Network Administration',1195,130);
INSERT INTO Course
VALUES (310,'Operating Systems',1195,NULL);
INSERT INTO Course
VALUES (142,'Project Management',1195,20);
INSERT INTO Course
VALUES (140,'Systems Analysis',1195,20);
INSERT INTO Course
VALUES (130,'Intro to Unix',1195,310);
INSERT INTO Course
VALUES (25,'Intro to Programming',1195,140);
INSERT INTO Course
VALUES (20,'Intro to Information Systems',1195,NULL);
INSERT INTO Course
VALUES (20,'Intro to Information Systems',1195,NULL);

-- Section TABLE
INSERT INTO Section
VALUES (81,20,2,'7/24/2007 9:30','L210',103,15);
INSERT INTO Section
VALUES (86,25,2,'6/10/2007 9:30','L210',107,15);
INSERT INTO Section
VALUES (89,25,5,'5/15/2007 9:30','L509',103,25);
INSERT INTO Section
VALUES (92,25,8,'6/13/2007 9:30','L509',106,25);
INSERT INTO Section
VALUES (104,330,1,'7/14/2007 10:30','L511',104,25);
INSERT INTO Section
VALUES (119,142,1,'7/14/2007 9:30','L211',103,25);
INSERT INTO Section
VALUES (155,122,4,'5/4/2007 9:30','L210',107,15);
INSERT INTO Section
VALUES (155,122,4,'5/4/2007 9:30','L210',107,15);

-- StudentID TABLE
INSERT INTO StudentID
VALUES (102,'Mr.','Fred','Crocitto','101-09 120th St.',718-555-5555,'Albert Hildegard Co.',1/22/2007,11419);
INSERT INTO StudentID
VALUES (103,'Ms.','J.','Landry','7435 Boulevard East #45',201-555-5555,'Albert Hildegard Co.',1/22/2007,7047);
INSERT INTO StudentID
VALUES (104,'Ms.','Laetia','Enison','144-61 87th Ave',718-555-5555,'Albert Hildegard Co.',1/22/2007,11435);
INSERT INTO StudentID
VALUES (105,'Mr.','Angel','Moskowitz','320 John St.',201-555-5555,'Alex. & Alexander',1/22/2007,7024);
INSERT INTO StudentID
VALUES (163,'Ms.','Nicole','Gillen','4301 N Ocean #103,904-555-5555','Oil of America Corp.',2/2/2007,10025);
INSERT INTO StudentID
VALUES (223,'Mr.','Frank','Pace','13 Burlington Dr.',203-555-5555,'Board Utilities',2/8/2007,10025);
INSERT INTO StudentID
VALUES (399,'Mr.','Jerry','Abdou','460 15th St. #4',718-555-5555,'Health Mgmt.Systems',2/23/2007,10025);
INSERT INTO StudentID
VALUES (400,'Mr.','Andy','Circus','460 15th St. #5',718-555-5555,'Health Mgmt.Systems',2/23/2007,10025);

-- Enrollment TABLE
INSERT INTO Enrollment
VALUES (102,86,1/30/2007,'B');
INSERT INTO Enrollment
VALUES (102,89,1/30/2007,'A');
INSERT INTO Enrollment
VALUES (103,81,1/30/2007,'');
INSERT INTO Enrollment
VALUES (104,81,1/30/2007,'A');
INSERT INTO Enrollment
VALUES (163,92,2/10/2007,'');
INSERT INTO Enrollment
VALUES (223,104,2/16/2007,'C');
INSERT INTO Enrollment
VALUES (223,119,2/16/2007,'');
INSERT INTO Enrollment
VALUES (223,119,2/16/2007,'');









